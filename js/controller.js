var runAlgorithm = function(){
  var textArea = document.getElementById("input");
  var output = document.getElementById("box-output");

  var alg = document.querySelector('input[name = "alg"]:checked');

  if(alg === undefined || alg === null ){
    alert("Selecione um algoritimo de escalonamento.");
    return;
  }

  var inputs = prepareInput(textArea);

  if(alg.value == "spn"){
    writeOutput(output, runSPN(inputs));
  }
  else{
    writeOutput(output, runLoteria(inputs));
  }

}

var prepareInput = function(textArea){
  var inputs = textArea.value.split(/\r?\n\n/g);

  return inputs
}


var writeOutput = function(output, value){
  output.innerHTML = "<p>Saída: </p>";

  value.forEach(function(element, index){
    output.innerHTML += "<p>" + (index+1) +". " + element.id + " " + element.completionTime +
    " " + element.timeSpentToIO + " " + element.timeSpentAsReady;
  })
}

var getArrivedProcess = function(currentTime, inputs){

  var arrayOfProcess = [];

  inputs.forEach(function(element, index){
    // console.log("el: " + element + " index: " + index);
    var processAsArray = element.split(" ");

    if(processAsArray[0] == currentTime){
      var processObj = createProcess(processAsArray, index);
      arrayOfProcess.push(processObj);
    }
  });

  console.log("ready Queue: " + arrayOfProcess);

  return arrayOfProcess;
}
