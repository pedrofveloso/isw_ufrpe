var blockedProcessQueue = [];
var readyToRunQueue = [];
var runningProcess;
var currentTime = 0;

var runSPN = function(inputs){
  readyToRunQueue = getArrivedProcess(currentTime, inputs);

  do{
    //selecionar processo.
    runningProcess = selectAvailableProcess(readyToRunQueue, currentTime);

    //verificar processo de i/o
    verifyIOProcess(runningProcess, currentTime);

    //executar ciclo do processo.

    //incrementar ciclo de processObj

    //adicionar processos entrantes

  }while(readyToRunQueue.length <= 0)

  return "SPN";
}

var selectAvailableProcess = function(readyToRunQueue, currentTime){
  var processToRun;
  readyToRunQueue.forEach(function(element, index){
    if(element.arrive == currentTime){
      if(processToRun !== null && (element.timeSpentAsReady < processToRun.timeSpentAsReady)){
        processToRun = element;
      }
    }
  });

  return processToRun;
}

var verifyIOProcess = function(runningProcess, currentTime){
  if(isProcessBlockedByIO(runningProcess, currentTime)){
    runningProcess.status = 1;
    if(blockedProcessQueue.indexOf(runningProcess) !== -1){
      blockedProcessQueue.push(runningProcess);
    }
    readyToRunQueue.splice(readyToRunQueue.indexOf(runningProcess), 1);
  }
  else{
    runningProcess.status = 0;
    if(blockedProcessQueue.indexOf(runningProcess) !== -1){
      blockedProcessQueue.splice(blockedProcessQueue.indexOf(runningProcess), 1);
    }
    readyToRunQueue.push(runningProcess);
  }
}
