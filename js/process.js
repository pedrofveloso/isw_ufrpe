var createProcess = function(element, identifier){
  var process = {};
  process["id"] = identifier;
  process["arrive"] = element[0];
  process["timeSpentToComputeProcess"] = element[1];
  process["ioSchedule"] = generateIOSchedule(element[2]);
  process["timeSpentToIO"] = 0;
  process["piority"] = element[3];
  process["timeSpentAsReady"] = 0;
  process["status"] = 0
  process["completionTime"] = 0
  return process;
}

var setProcessStatus = function(element, statusCode){
  element.status = statusCode;
  setProcessTimes(element);
}

var setProcessTimes = function(element){
  if(element.status == 0){ //pronto
    element.readyTime += 1;
  }
  else if (element.status == 1) { //bloquado
    element.ioTime += 1;
  }
}

var generateIOSchedule = function(ioSchecule){
  var string = ioSchecule.replace(/[^0-9;:]/g, "");
  string = string.split(";");
  var ret = []
  string.forEach(function(element){
    ret.push(element.split(":"));
  })

  return ret;
}

var isProcessBlockedByIO = function(runningProcess, currentTime){
  var isBlocked = false;
  runningProcess.ioSchecule.forEach(function(io){
    if(io[0] >= currentTime + runningProcess.arrive){
      isBlocked = true;
      break;
    }
    if(io[1] == currentTime + runningProcess.arrive){
      isBlocked = false;
    }
  });
  return isBlocked;
}
