var ticketsMap = {}; // mapa ticket x processo
var ticketsArray = []; //array para loteria
var blockedProcessQueue = []; //fila de bloqueados por E/S

var output = [];

var runLoteria = function(inputs){
  var currentTime = 0;
  var quantum = 2;
  var readyProcessQueue = getArrivedProcess(currentTime, inputs);
  var ticketToRun;

  do{
    //atribuir  tickets
    ticketsArray = distributeTickets(readyProcessQueue, ticketsArray, ticketsMap);

    //selecionar ticket
    if( currentTime%quantum == 0 ){
      ticketToRun = selectAvailableTicketProcess(ticketsArray);
    }

    //Executar e remover finalizados
    readyProcessQueue = executeProcessUsingQuantum(ticketToRun, readyProcessQueue, currentTime);

    //incrementar currentTime
    currentTime += 1;

    //pegar novos processos
    readyProcessQueue = readyProcessQueue.concat(getArrivedProcess(currentTime, inputs));
  }while(readyProcessQueue.length > 0);

  //retornar lista de saida
  return output;
}

var distributeTickets = function(processQueue, ticketsArray, ticketsMap){

  var arrayOfProcess = Object.keys(ticketsMap).map(function(key){
    return ticketsMap[key];
  });

  processQueue.forEach(function(element){
    if(arrayOfProcess.indexOf(element) == -1){
      console.log("process: " + element);
      var ticket = generateTicket(ticketsMap);
      ticketsMap[ticket] = element;
      var tickets = Array(element.priority).fill(ticket);
      ticketsArray = ticketsArray.concat(tickets);
    }
  });

  return ticketsArray;
}

var generateTicket = function(ticketsMap){
  var ticket = Math.floor((Math.random() * 9999) + 1);
  if( ticketsMap.hasOwnProperty(ticket)){
      return generateTicket(ticketsMap);
  }

  return ticket;
}

var selectAvailableTicketProcess = function(ticketsArray){
  var processIndex = Math.floor((Math.random() * ticketsArray.length-1) + 1);

  return ticketsArray[processIndex];
}

var executeProcessUsingQuantum = function(ticketToRun, readyProcessQueue, currentTime){
  var processToRun = this.ticketsMap[ticketToRun];
  var timeToFinalize = parseInt(processToRun.timeSpentToComputeProcess) - 1;
  processToRun.timeSpentToComputeProcess = timeToFinalize;

  if(timeToFinalize <= 0){
    processToRun.completionTime = currentTime;
    processToRun.timeSpentAsReady = Math.abs(processToRun.arrive - currentTime);
    readyProcessQueue = removeTicketFromQueues(ticketToRun, processToRun, readyProcessQueue);
  }

  return readyProcessQueue;
}

var removeTicketFromQueues = function(ticketToRun, processToRun, readyProcessQueue){
  output.push(processToRun);
  readyProcessQueue.splice(readyProcessQueue.indexOf(processToRun), 1);
  delete this.ticketsMap[ticketToRun];
  this.ticketsArray = ticketsArray.filter(function(el){return el !== ticketToRun})

  this.ticketToRun = selectAvailableTicketProcess(this.ticketsArray);

  return readyProcessQueue
}
